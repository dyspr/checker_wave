var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var arraySize = 7
var tileArray = create2DArray(arraySize, arraySize, 0, true)
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < tileArray.length - 1; i++) {
    for (var j = 0; j < tileArray[i].length - 1; j++) {
      fill(255)
      noStroke()
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(arraySize * 0.5) + 0.5) * boardSize * (1 / (arraySize + 2)), windowHeight * 0.5 + (j - Math.floor(arraySize * 0.5) + 0.5) * boardSize * (1 / (arraySize + 2)))
      rotate(Math.PI * 0.5 * frame)
      rect(0, 0, 0.8 * boardSize * (1 / (arraySize + 2) / sqrt(2)) * sin(frame * 2 - (i - j) * (1 / arraySize)), 0.8 * boardSize * (1 / (arraySize + 2) / sqrt(2)) * sin(frame * 2 - (i - j) * (1 / arraySize)))
      pop()
    }
  }

  for (var i = 0; i < tileArray.length; i++) {
    for (var j = 0; j < tileArray[i].length; j++) {
      fill(255)
      noStroke()
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(arraySize * 0.5)) * boardSize * (1 / (arraySize + 2)), windowHeight * 0.5 + (j - Math.floor(arraySize * 0.5)) * boardSize * (1 / (arraySize + 2)))
      rotate(Math.PI * 0.5 * frame * (-1))
      rect(0, 0, 0.8 * boardSize * (1 / (arraySize + 2) / sqrt(2)) * sin(1 - frame * 2 + (i - j) * (1 / arraySize)), 0.8 * boardSize * (1 / (arraySize + 2) / sqrt(2)) * sin(1 - frame * 2 + (i - j) * (1 / arraySize)))
      pop()
    }
  }

  frame += deltaTime * 0.0002
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
